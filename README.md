# Bayesian Successor Representation
Bayesian Successsor Representation is a flexible reinforcement learning model, combining approximate Bayesian nonparametrics with the adaaptive, factorized value functions provided by the Successor Representation. The paper can be found at https://arxiv.org/pdf/1906.07663.pdf

The code for the continuous state space requires theano and lasagne installations.

All code is written in python 2.7.

