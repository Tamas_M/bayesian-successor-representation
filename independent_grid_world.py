import numpy as np

MAPS = {
    "chain": [
        "GFFFFFFFFFFFFFSFFFFFFFFFFFFFG"
    ],
    "4x4_very_safe":
        [
        "SFFF",
        "FFFF",
        "FFFF",
        "FFFG"
    ],
    "4x4_safe": [
        "SFFF",
        "FWFW",
        "FFFW",
        "WFFG"
    ],
            "4x4_safe_2": [
        "SFFF",
        "FWFW",
        "FFFW",
        "FFFG"
    ],
    "4x4": [
        "SFFF",
        "FHFH",
        "FFFH",
        "HFFG"
    ],
    "5x5_safe":[
             "SFFFW",
             "FWFFF",
             "FFFWW",
             "WFWFF",
             "FFFFG"],

    "5x5_empty":[
             "SFFFF",
             "FFFFF",
             "FFFFF",
             "FFFFF",
             "FFFFG"],



    "6x6_safe":[
        "SFFFWF",
        "FWFFFF",
        "FFFWWF",
        "FFFFFF",
        "FWFFFW",
        "FFFWFG"],



    "7x7_safe":[
                "SFFFFWF",
                "FFFFFFF",
                "FWWFFFF",
                "FWWWFFF",
                "FFFWFWW",
                "WFWFFFF",
                "FFFFWFG"
                
                ],
    "7x7_empty":[
                "SFFFFFF",
                "FFFFFFF",
                "FFFFFFF",
                "FFFFFFF",
                "FFFFFFF",
                "FFFFFFF",
                "FFFFFFG"
                
                ],

            
            
            
    "8x8": [
        "SFFFFFFF",
        "FFFFFFFF",
        "FFFHFFFF",
        "FFFFFHFF",
        "FFFHFFFF",
        "FHHFFFHF",
        "FHFFHFHF",
        "FFFHFFFG"
    ],
        
            "8x8_safe": [
        "SFFFFFWF",
        "FFFFFFFF",
        "FFWWFFFF",
        "FFFFFWFF",
        "FFWWWFFF",
        "FFFFWFWW",
        "WFFWFFFF",
        "FFFFFWFG"
    
],
               "8x8_empty": [
        "SFFFFFFF",
        "FFFFFFFF",
        "FFFFFFFF",
        "FFFFFFFF",
        "FFFFFFFF",
        "FFFFFFFF",
        "FFFFFFFF",
        "FFFFFFFG"
    
],           
                    
                    
                    
                  "15x15_empty": [
        "SFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFF",
        "FFFFFFFFFFFFFFG"
    
],   
                          "4_maze":[
        
                                  
        
        "FWWWF",
        "FWWWF",
        "FFFFF",
        "FWFWF",
        "FWSWF"
                                  
                                  ],
                                  "moser":[
                                          
             "SFFFFWWFFFFF",
             "FFFFFWWFFFFF",
             "FFFFFFFFFFFF",
             "FFFFFWWFFFFF",
             "FFFFFWWFFFFF"
                    
                                          ],
                    
}


class GridWorldEnv(object):
    """
    S : starting point
    F : free space
    W : wall
    H : hole (terminates episode)
    G : goal


    """

    def __init__(self, desc='4x4',action_dim=4):
         
        desc = MAPS[desc]
        self.desc = np.array(list(map(list, desc)))
        desc=self.desc
        
        self.n_row, self.n_col = desc.shape
        (start_x,), (start_y,) = np.nonzero(desc == 'S')
        self.start_state = start_x * self.n_col + start_y
        self.state = None
        self.domain_fig = None
        self.observation_space=list(range(self.n_row*self.n_col))
        self.action_space=list(range(action_dim))

    def reset(self):
        self.state = self.start_state
        return self.state

   
   

    def step(self, action,use_terminal=1):
        """
        action map:
        0: left
        1: down
        2: right
        3: up
        :param action: should be a one-hot vector encoding the action
        :return:
        """
        possible_next_states = self.get_possible_next_states(self.state, action)

        probs = [x[1] for x in possible_next_states]
        next_state_idx = np.random.choice(len(probs), p=probs)
        next_state = possible_next_states[next_state_idx][0]

        next_x = int(next_state / self.n_col)
        next_y = int(next_state % self.n_col)

        next_state_type = self.desc[next_x, next_y]
        if next_state_type == 'H':
            done = True
            reward = 0
        elif next_state_type in ['F', 'S','P']:
            done = False
            reward = 0
        elif next_state_type == 'G' and use_terminal:
            done = True
            reward = 1
        else:
            raise NotImplementedError
        self.state = next_state
        return (self.state, reward, done)

    def get_possible_next_states(self, state, action):
        """
        Given the state and action, return a list of possible next states and their probabilities. Only next states
        with nonzero probabilities will be returned
        :param state: start state
        :param action: action
        :return: a list of pairs (s', p(s'|s,a))
        """
        assert state in self.observation_space
        assert action in self.action_space

        x = int(state / self.n_col)
        y = int(state % self.n_col)
        coords = np.array([x, y])

        increments = np.array([[0, -1], [1, 0], [0, 1], [-1, 0]])
        next_coords = np.clip(
            coords + increments[action],
            [0, 0],
            [self.n_row - 1, self.n_col - 1]
        )
        next_state = next_coords[0] * self.n_col + next_coords[1]
        state_type = self.desc[x, y]
        next_state_type = self.desc[next_coords[0], next_coords[1]]
        if next_state_type == 'W' or state_type == 'H' or state_type == 'G':
            return [(state, 1.)]
        else:
            return [(next_state, 1.)]

   