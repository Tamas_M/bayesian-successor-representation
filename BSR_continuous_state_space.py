
from __future__ import division
from random import sample, randint, random
from time import time, sleep
import numpy as np
import scipy
from scipy.stats import norm
import __main__ as main
from math import sin, cos, tan, asin, acos, atan, pi
import datetime
import __main__ as main
import lasagne
from lasagne.updates import rmsprop
from lasagne.init import HeUniform, Constant
import lasagne.layers as L
from lasagne.layers import Conv2DLayer, InputLayer, DenseLayer, get_output, \
    get_all_params, get_all_param_values, set_all_param_values, DropoutLayer, LSTMLayer, FlattenLayer, ReshapeLayer, SliceLayer
from lasagne.nonlinearities import rectify, tanh
from lasagne.objectives import squared_error
import theano
from theano import tensor
import theano.tensor as T
from tqdm import trange
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.lines as mlines
from continuous_maze import conti_env
a = np.asarray
epsilon = 10**(-20)
target_network_update = 5
tabular = 0
phi_tabular = 0

# =============================================================================
# Environment Params
# =============================================================================


env = conti_env(
    maze_length=3.,
    n_rewards=1,
    n_place_cells=100,
    place_cov=0.1 *
    np.eye(2),
    reward_values=[10.],
    rewards_markov=[1],
    reward_distance=[0.25],
    step_size_=0.3,
    add_wall=1)
action_dim = 4
change_environment = 30
step_penalty = 0.0
max_steps_per_episode = 150
puddles = 0
puddle_penalty = -1.
give_w = 0
learn_w = 1
gamma = 0.99
gamma_cum_state = 0.9
env_dim = env.n_dim
obs_dim = env_dim
resolution = a([env_dim])

# =============================================================================
# Filtering_params
# =============================================================================
'''maximum number of cluster'''
MC = 4

start_filtering = 0 * 1e10
start_wcr_update = 0
wcr_update = 1
filter_delay = 4
filter_together = 1
alpha = 2.

# =============================================================================
# CR_params
# =============================================================================
lr_cr = .005
w_cr_decay_rate = 4000.
cr_lr_min = 0.001
wcr_decay = 1
w_set_off_rate = 0.025
set_off_w = 1
particle_sd = 1.6


# =============================================================================
# Network params
# =============================================================================
phi_dim = env.size
phi_pos_size = env.n_place_cells
phi_shape = [-1, phi_dim]
mlp_hid_size = 150
phi_hid_size = 30
phi_clip_lb, phi_clip_ub = a([-1, 1]) * 0.005
succ_clip_lb, succ_clip_ub = a([-1, 1]) * 0.01
dropout_rate = 0.1

# =============================================================================
# Learning_params
# =============================================================================

reward_convolve = (gamma * 0.9)**a([3, 2, 1, 0, 1, 2, 3])
network_synch_rate = 80
n_particles = 100
len_particles = 50
direct_update = 1
replay_update = 1
memory_replay_size = 35
use_terminal_in_replay = 0
succ_replay_length = 15
w_lr = 0.005
succ_lr = 0.00025
phi_lr = 0.0002
epochs = 10
replay_memory_size = 200


def list_flatten(l): return [item for sublist in l for item in sublist]


def m1h(row, column, sizes):
    oh = np.zeros(sizes).astype('int32')
    oh[row, column] = 1
    return oh


def f1h(row, column, sizes):

    return np.ndarray.flatten(m1h(row, column, sizes))


def f1hs(state_, iter=1):
    for _ in xrange(iter):
        state_ = np.expand_dims(state_, 0)

    return state_


def ss(s_):
    return f1hs(f1h(0, s_, [1, env_dim]), 2)


def randargmax(b, axis_=1, squeeze=1, **kw):
    if squeeze:
        b = np.squeeze(b)
    if b.ndim <= 1:
        return np.argmax(np.random.random(b.shape) * (b == b.max()), **kw)
    else:
        return np.argmax(np.random.random(b.shape) *
                         (b == b.max(axis=axis_, keepdims=True)), axis=axis_)


def softmax(x, theta):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x * theta - np.max(x))
    return e_x / e_x.sum()

def save_results_a(filename=str(main.__file__)+'Results'+str(datetime.datetime.now())[:10]+str(int(np.random.rand()*1000))):
    params=[change_environment,gamma,lr_cr,MC,set_off_w,g_epsilon,particle_sd,]
    data=[All_Episode_lengths,All_Returns,All_Weightsr,All_all_rewards,All_wsors,All_epsilons,params]
    np.savez(filename,*data)
    
def ppath(k_):
    plt.xlim(0, env.maze_length)
    plt.ylim(0, env.maze_length)
    ax = plt.gca()
    for wall in env.walls:
        l = mlines.Line2D([wall[0][0], wall[0][0] + wall[1][0]],
                          [wall[0][1], wall[0][1] + wall[1][1]])
        ax.add_line(l)
    plt.plot(paths[k_]['env_rewards'][0][0], paths[k_]
             ['env_rewards'][0][1], 'o', c='green')
    plt.plot(paths[k_]['env_rewards'][1][0], paths[k_]
             ['env_rewards'][1][1], 'x', c='black')
    plt.plot(paths[k_]['pos'][0][0], paths[k_]['pos'][0][1], 'o', c='blue')
    steps = len(paths[k_]['observations']) + 1
    fs = np.arange(1, steps + 1) / (1.2 * a(steps + 1, dtype='float'))
    plt.scatter(a(paths[k_]['pos'])[:, 0], a(
        paths[k_]['pos'])[:, 1], c=cm.hot(fs))
    plt.show()


def get_new_particle(assignment):
    l = len(assignment)

    particles_ = list(set(assignment))
    pl = len(particles_)
    new_prob = alpha / (alpha + l)

    counts = [assignment.count(k) for k in particles_]
    if pl < MC:
        probs = np.append((a(counts) / (l + alpha)), [new_prob])
        particles_.append(
            min(list(set(range(MC)).difference(set(assignment)))))
    else:
        probs = (a(counts) + alpha / pl) / (l + alpha)

    #print probs
    new_particle = np.random.choice(particles_, p=probs)
    assignment = np.append(a(assignment), [new_particle])
    return new_particle.astype('int32')


def particle_filter(weights_f, particles_):
    new_particles = []
    for k in xrange(n_particles):
        new_particle = get_new_particle(np.ndarray.tolist(particles_[k, :]))
        new_particles.append(new_particle)

    weights_e = weights_f  # +np.max([-20-np.min(a(weights_)),a(0)]))
    weights_p = weights_e / np.sum(weights_e)
    if np.any(np.isnan(weights_p)):
        nans.append([learning_episode, weights_f, weights_e, weights_p])
        weights_p = f1h(0, randargmax(weights_p), [1, MC])
    unique, counts = np.unique(new_particles, return_counts=True)
    Weights_.append(weights_f)
    counts_t = np.zeros(MC)
    counts_t[unique] = counts
    total_particle_weights = weights_p * counts_t

    weights_t = total_particle_weights / np.sum(total_particle_weights)
    particle_weights = weights_p[new_particles] / \
        np.sum(total_particle_weights)
    particles_ = np.concatenate(
        (particles_, a(new_particles).reshape([n_particles, 1])), axis=1)
    if particles_.shape[1] > len_particles:
        particles_ = np.delete(particles_, 0, 1)

    indices = np.random.choice(n_particles, n_particles, p=particle_weights)
    particles_resampled = particles_[indices[:]]

    return weights_t, particles_resampled


def nan_check(networks):
    if np.any(np.isnan(w.get_value())):
        print('w is nan!')
        sleep(2)
    for network in networks:
        if np.any(np.isnan(get_all_params(network)[0].get_value())):
            print(network, ' is nan!!!')
            sleep(1)


def create_state_networks():

    state_sym = T.tensor3('state_sym')
    phi_sym = T.matrix('phi_sym')
    weights_sym = T.vector('weights_sym')
    context_sym = T.iscalar('context_sym')
    reward_sym = T.matrix('reward_sym')
    reward_cr_sym = T.vector('c_reward_cr_sym')
    cr_lr_sym = T.scalar('cr_lr_sym')
    w_lr_sym = T.scalar('w_lr_sym')
    cr_reg_sym = T.scalar('cr_reg_sym')

    
    def build_recurrent(input_var=state_sym):
            l_in = lasagne.layers.InputLayer(shape=(None, None, resolution[0]),
                                             input_var=input_var)
            return l_in, l_in
    

    def features_sym(network, state_):
        return get_output(network, inputs=state_)

    phi_n = build_recurrent()
    phi_n_2 = build_recurrent()
    latents = features_sym(phi_n, state_sym)[1]
    s_features_ = features_sym(phi_n, state_sym)[0]
    s_2_features_ = features_sym(phi_n_2, state_sym)[0]
    get_s_features = theano.function(
        [state_sym], [
            s_features_, latents], name="state_embedding_fn")
    get_s_2_features = theano.function(
        [state_sym], s_2_features_, name="state_2_embedding_fn")
    cost_cr = T.pow(((T.repeat(reward_cr_sym, MC) - T.tensordot(w_cr,
                                                                s_features_, [[1], [1]])) * weights_sym).norm(L=2), 2) / MC
    reward_cr_s = T.tensordot(w_cr, s_features_, [[1], [1]]).reshape([MC, -1])
    errors_cr = squared_error(T.tile(reward_cr_sym, MC).reshape(
        [MC, -1]), reward_cr_s) * T.repeat(weights_sym, reward_cr_sym.shape[0]).reshape([MC, -1])
    cost_cr = errors_cr.mean()

    reward_s = T.tensordot(w, s_features_.reshape([state_sym.shape[0], state_sym.shape[1], -1])[
                           :, -reward_sym.shape[1]:, :], [[1], [2]]).reshape([MC, -1])
    errors_r = squared_error(
        T.tile(T.flatten(reward_sym), MC).reshape([MC, -1]), reward_s)

    reward_s_phi = T.tensordot(w, phi_sym, [[1], [1]]).reshape([MC, -1])
    errors_r_phi = squared_error(
        T.tile(T.flatten(reward_sym), MC).reshape([MC, -1]), reward_s_phi)
    cost_r_phi = errors_r_phi.mean() + 0.001 * w.norm(L=1)

    reward_cr_s_phi = T.tensordot(w_cr, phi_sym, [[1], [1]]).reshape([MC, -1])
    errors_cr_phi = squared_error(T.tile(T.flatten(reward_cr_sym), MC).reshape(
        [MC, -1]), reward_cr_s_phi) * T.repeat(weights_sym, reward_cr_sym.shape[0]).reshape([MC, -1])
    cost_cr_phi = errors_cr_phi.mean() + cr_reg_sym * \
        w_cr.norm(L=2, axis=1)[context_sym]

    cr_params = []
    cr_params.append(w_cr)
    cr_grads = T.grad(cost=cost_cr, wrt=cr_params)
    cr_phi_grads = T.grad(cost=cost_cr_phi, wrt=cr_params)

    w_params = []
    w_params.append(w)
    w_phi_grads = T.grad(cost_r_phi, wrt=w_params)

    update_w_phi = theano.function(
        inputs=[
            phi_sym,
            reward_sym,
            w_lr_sym],
        updates=lasagne.updates.rmsprop(
            w_phi_grads,
            w_params,
            learning_rate=w_lr_sym),
        outputs=[
            cost_r_phi,
            errors_r_phi],
        on_unused_input='ignore',
        name='w_learn_phi_fn')

    update_cr = theano.function(
        inputs=[
            state_sym,
            reward_cr_sym,
            weights_sym,
            cr_lr_sym],
        updates=lasagne.updates.rmsprop(
            cr_grads,
            cr_params,
            learning_rate=cr_lr_sym),
        outputs=[
            cost_cr,
            errors_cr],
        on_unused_input='ignore',
        name='cr_learn_fn')

    update_cr_phi = theano.function(
        inputs=[
            phi_sym,
            reward_cr_sym,
            weights_sym,
            cr_lr_sym,
            cr_reg_sym,
            context_sym],
        updates=lasagne.updates.rmsprop(
            cr_phi_grads,
            cr_params,
            learning_rate=cr_lr_sym),
        outputs=[
            cost_cr_phi,
            errors_cr_phi],
        on_unused_input='ignore',
        name='cr_learn_phi_fn')

    def update_phi(): return []
    get_reward_fun = theano.function([state_sym, reward_sym, weights_sym], [
                                     reward_s, errors_r], on_unused_input='ignore', name="get_reward_fn")
    get_cr_fun = theano.function([state_sym, reward_cr_sym, weights_sym], [
                                 reward_cr_s, errors_cr], name="get_cr_fn")
    return update_w_phi, update_cr, update_cr_phi, update_phi, get_s_features, get_s_2_features, get_reward_fun, get_cr_fun


def create_succ_networks(action_dim, phi_dim):
    state_phi_sym = T.tensor3('state_phi_sym')

    target_sym = T.tensor3('target_sym')
    weight_sym = T.scalar('weight_sym')

    action_sym = T.imatrix('action_sym')

    sf_lr_sym = T.scalar('sf_lr_sym')

    cut_gradient_sym = T.iscalar('cut_gradient_sym')

    def build_mlp(input_var=state_phi_sym):
        l_in = lasagne.layers.InputLayer(shape=(None, None, phi_dim),
                                         input_var=input_var)

        l_in_drop = DropoutLayer(l_in, p=dropout_rate)
        l_reshape1 = ReshapeLayer(l_in_drop, (-1, phi_dim))
        l_hid1 = DenseLayer(
            l_reshape1, num_units=mlp_hid_size,
            nonlinearity=tanh,
            W=lasagne.init.GlorotUniform())
        l_hid1_drop = DropoutLayer(l_hid1, p=dropout_rate)

        l_hid2 = DenseLayer(
            l_hid1_drop, num_units=mlp_hid_size,
            nonlinearity=tanh,
            W=lasagne.init.GlorotUniform())
        l_hid2_drop = DropoutLayer(l_hid2, p=dropout_rate)

        l_out = DenseLayer(
            l_hid2_drop, num_units=phi_dim * action_dim,
            nonlinearity=None)

        l_out_reshape = ReshapeLayer(
            l_out, (input_var.shape[0], -1, phi_dim * action_dim))
        return l_out_reshape

    def features_sym(network, state_):
        return T.flatten(get_output(network, inputs=state_))

    networks = []

    sfs = build_mlp()
    sfs_2 = build_mlp()
    networks.append(sfs)
    succ_params = get_all_params(sfs)
    sfe = get_output(sfs, inputs=state_phi_sym)
    successor_features_ = sfe.reshape(
        [sfe.shape[0], sfe.shape[1], action_dim, phi_dim])
    successor_features_h = successor_features_[:, cut_gradient_sym:, :, :]

    target_successor_features, updates = theano.scan(fn=lambda action_t, target_t, k_pos, prior_target: T.set_subtensor(prior_target[k_pos, tensor.arange(
        successor_features_h.shape[1]), action_t, :], target_t), outputs_info=successor_features_h, sequences=[action_sym, target_sym, tensor.arange(action_sym.shape[0])])

    errors_s = squared_error(successor_features_h,
                             target_successor_features[-1])
    cost_s_ = errors_s.mean()
    succ_grads = [
        T.clip(
            g,
            succ_clip_lb,
            succ_clip_ub) for g in T.grad(
            cost=cost_s_,
            wrt=succ_params,
            disconnected_inputs='ignore')]

    s_2 = get_output(sfs_2, inputs=state_phi_sym)

    update_succ = theano.function(
        inputs=[
            state_phi_sym,
            action_sym,
            target_sym,
            cut_gradient_sym,
            weight_sym,
            sf_lr_sym],
        updates=lasagne.updates.rmsprop(
            succ_grads,
            succ_params,
            learning_rate=sf_lr_sym *
            weight_sym),
        outputs=[
            cost_s_,
            errors_s],
        on_unused_input='ignore')

    get_successors = theano.function(
        inputs=[state_phi_sym], outputs=[sfe, s_2])

    return sfs, sfs_2, get_successors, update_succ


def pfilter(
        robservations,
        rrewards,
        terminal,
        s_reward_,
        w_cr_real,
        particles):
    weights_ = []

    if not terminal:
        filter_ = 1

        if len(rrewards) >= filter_delay:
            s_state = robservations[-filter_delay]

            s_reward = s_reward_[-filter_delay]
            # s_state=robservations[-3]
            # s_reward=rrewards[-3]

            # if not filtered_already[s_state,0] or s_reward !=
            # filtered_already[s_state,1]:
            for k in xrange(MC):
                weights_.append(norm.pdf(s_reward, np.dot(get_s_features(f1hs(s_state))[
                                0].reshape(phi_dim), w_cr_real[k, :]), particle_sd))
            # filtered_already[s_state]=1

            weights, particles = particle_filter(weights_, particles)
            if wcr_update:
                weights_nl_2 = f1h(
                    0, randargmax(weights), [
                        1, MC]).astype(
                    theano.config.floatX)
                cost_cr, errors_cr = update_cr_phi(s_state, a(
                    [s_reward]), weights_nl_2, cr_lr, reg_cr_phi, np.argmax(weights_nl_2))
                Errors_cr.append([cost_cr, errors_cr])

    elif terminal:
        filter_ = 1

        s_reward = s_reward_[-min(len(rrewards), 3):]


        cr_list = []

        for k in xrange(-len(s_reward), 0):
            cr_list.append([robservations[k], s_reward_[k]])

        for k in xrange(MC):
            weights_.append([(norm.pdf(el[1], np.dot(get_s_features(f1hs(el[0]))[0].reshape(
                phi_dim), w_cr_real[k, :]), particle_sd)) for el in cr_list if np.all(el[0] != 8)])
        if filter_together:

            weights, particles = particle_filter(
                np.prod(weights_, axis=1), particles)
            if wcr_update:
                weights_nl_2 = f1h(
                    0, randargmax(weights), [
                        1, MC]).astype(
                    theano.config.floatX)
                for el in cr_list:
                    cost_cr, errors_cr = update_cr_phi(el[0], f1hs(
                        el[1]), weights_nl_2, cr_lr, reg_cr_phi, np.argmax(weights_nl_2))
                    Errors_cr.append([cost_cr, errors_cr])

        else:
            for k_s in xrange(len(weights_[0])):
                weights, particles = particle_filter(
                    a(weights_)[:, k_s], particles)
                if wcr_update:
                    weights_nl_2 = f1h(
                        0, randargmax(weights), [
                            1, MC]).astype(
                        theano.config.floatX)
                    cost_cr, errors_cr = update_cr_phi(cr_list[k_s][0], f1hs(
                        cr_list[k_s][1]), weights_nl_2, cr_lr, reg_cr_phi, np.argmax(weights_nl_2))
                    Errors_cr.append([cost_cr, errors_cr])
    else:
        filter_ = 0

    return weights, particles, filter_


class ReplayMemory:
    def __init__(self, capacity, context):
        state_shape = capacity + [env_dim]
        phi_m_shape = capacity + [phi_dim]
        self.context = context
        self.s1 = np.zeros(state_shape, dtype=np.float32)
        self.s2 = np.zeros(state_shape, dtype=np.float32)
        self.s1_phi = np.zeros(phi_m_shape, dtype=np.float32)
        self.s2_phi = np.zeros(phi_m_shape, dtype=np.float32)
        self.cum_state = np.zeros(state_shape, dtype=np.float32)
        self.ac = np.zeros(capacity, dtype=np.int32)
        self.r = np.zeros(capacity, dtype=np.float32)
        self.cr = np.zeros(capacity, dtype=np.float32)
        self.ep_lengths = np.zeros(capacity[0], dtype='int')
        self.priority = np.zeros(capacity, dtype=np.float32)
        self.priority_cr = np.zeros(capacity, dtype=np.float32)
        self.isterminal = np.zeros(capacity, dtype=np.bool_)
        self.sc = np.zeros(capacity, dtype=np.int32)
        self.capacity = capacity
        self.size = 0
        self.pos = [0, 0]
        self.pos_ep = 0

    def add_transition(
            self,
            s1,
            action,
            s2,
            s1_phi,
            s2_phi,
            cum_state,
            reward,
            s_reward,
            isterminal,
            sampled_context,
            new_episode=0):
        self.s1[self.pos[0], self.pos[1], :] = s1
        self.ac[self.pos[0], self.pos[1]] = action
        self.priority[self.pos[0], self.pos[1]] = np.max(self.priority)
        self.priority_cr[self.pos[0], self.pos[1]] = max(
            np.max(self.priority_cr), 1.)
        self.s2[self.pos[0], self.pos[1], :] = s2
        self.isterminal[self.pos[0], self.pos[1]] = isterminal
        self.r[self.pos[0], self.pos[1]] = reward
        self.cr[self.pos[0], self.pos[1]] = s_reward
        self.sc[self.pos[0], self.pos[1]] = sampled_context
        self.s1_phi[self.pos[0], self.pos[1], :] = s1_phi
        self.s2_phi[self.pos[0], self.pos[1], :] = s2_phi
        self.cum_state[self.pos[0], self.pos[1], :] = cum_state

    def get_sample(
        self,
        start_position,
        sample_size,
        pri,
        end_position=0,
        pri0_p=0,
    ):
        if end_position == 0:
            end_position = self.size + 1

        if pri == 0:
            # Randomly choose episodes, or weight them by pri0_p. Then chooses
            # consecutive steps. This is needed for recurrent replay

            i = []
            i_e = np.random.choice(self.size +
                                   1, sample_size, False, p=self.ep_lengths[:self.size +
                                                                            1]**pri0_p /
                                   ((self.ep_lengths[:self.size +
                                                     1]**pri0_p).sum()))
            for ep in i_e:
                if self.ep_lengths[ep] >= succ_replay_length:
                    i.append(np.random.choice(np.arange(succ_replay_length - 1, self.ep_lengths[ep]), p=self.priority[ep][succ_replay_length - 1:self.ep_lengths[ep]] / np.sum(
                        self.priority[ep][succ_replay_length - 1:self.ep_lengths[ep]])) + np.arange(-succ_replay_length + 1, 1) + ep * max_steps_per_episode)

            i = a(list_flatten(i))

        elif pri == 3:
            i = []
            # i_e = np.random.choice( self.size+1, sample_size[0],False)
            for ep in xrange(end_position):
                i.append(
                    np.random.choice(
                        self.ep_lengths[ep], min(
                            sample_size, self.ep_lengths[ep])))

            i = a(list_flatten(i))

        # Chosses from all transitions stored in memory form episodes between
        # start_position and end_position
        elif end_position > start_position:
            if pri == 1:

                i = np.random.choice(range(start_position * max_steps_per_episode,
                                           end_position * max_steps_per_episode),
                                     sample_size,
                                     False,
                                     p=self.priority[start_position:end_position,
                                                     :].reshape(-1) / (self.priority[start_position:end_position,
                                                                                     :].sum()))
            elif pri == 2:
                i = np.random.choice(range(start_position * max_steps_per_episode,
                                           end_position * max_steps_per_episode),
                                     sample_size,
                                     False,
                                     p=self.priority_cr[start_position:end_position,
                                                        :].reshape(-1) / self.priority_cr[start_position:end_position,
                                                                                          :].sum())

        else:
            if pri == 1:
                i = np.random.choice(range(end_position * max_steps_per_episode) + range(start_position * max_steps_per_episode,
                                                                                         self.size * max_steps_per_episode),
                                     sample_size,
                                     False,
                                     p=np.append(self.priority[:end_position,
                                                               :],
                                                 self.priority[start_position:self.size]) / np.append(self.priority[:end_position],
                                                                                                      self.priority[start_position:self.size]).sum())
            elif pri == 2:
                i = np.random.choice(range(end_position) + range(start_position,
                                                                 self.size),
                                     sample_size,
                                     False,
                                     p=np.append(self.priority_cr[:end_position],
                                                 self.priority_cr[start_position:self.size]) / np.append(self.priority_cr[:end_position],
                                                                                                         self.priority_cr[start_position:self.size]).sum())
        # i=a(i,dtype='int32')

        i_t = [(int(np.floor(i_ / 150)), int(i_ - np.floor(i_ / 150) * 150))
               for i_ in i]

        i = i_t
        return a([self.s1[k_] for k_ in i]), a([self.ac[k_] for k_ in i]), a([self.s2[k_] for k_ in i]), a([self.s1_phi[k_] for k_ in i]), a([self.s2_phi[k_]
                                                                                                                                              for k_ in i]), a([self.cum_state[k_] for k_ in i]), a([self.r[k_] for k_ in i]), a([self.cr[k_] for k_ in i]), a([self.isterminal[k_] for k_ in i]), i

    def get_last(self):
        last_pos = (self.pos[0], self.pos[1])
        return f1hs(
            self.s1[last_pos]), f1hs(
            self.ac[last_pos]), f1hs(
            self.s2[last_pos]), f1hs(
                self.s1_phi[last_pos]), f1hs(
                    self.s2_phi[last_pos]), f1hs(
                        self.r[last_pos]), f1hs(
                            self.cr[last_pos]), f1hs(
                                self.isterminal[last_pos])


def learn_from_cr(
        context_,
        weights_,
        start_position_,
        end_position_,
        replay_size_,
        learning_rate_cr_):
    reg_cr_phi = 0.0005
    s1, ac, s2, s1_phi, s2_phi, replay_cum_state, r, cr, isterminal, locs = memory_cr[context_].get_sample(
        start_position_, replay_size_, 2, end_position_)

    weights_nl_2 = f1h(0, context_, [1, MC]).astype(theano.config.floatX)
    cost_cr, errors_cr = update_cr_phi(
        s2_phi, cr, weights_nl_2, learning_rate_cr_, reg_cr_phi, context_)
    Errors_cr.append([cost_cr, errors_cr])
    for i in xrange(len(locs)):
        memory_cr[context_].priority_cr[locs[i]
                                        ] = 1.  # max(np.abs(errors_cr[context_,i] ),1e-5)


def learn_from_memory(context_, direct_, weights_, cum_state_, replay_size_=4):
   

    # Get a random minibatch from the replay memory and learns from it.

    if direct_ == 0:
        if (memory[context_].size >
                0 or memory[context_].ep_lengths[0] >= succ_replay_length):

            replay_size_ = min(
                succ_replay_length, len(
                    np.where(
                        memory[context_].priority > 0)[0]))
            if replay_size_ > 0:
                s1, ac, s2, s1_phi, s2_phi, replay_cum_state, r, cr, isterminal, locs = memory[context_].get_sample(
                    0, replay_size_, 1)
                both_succs = a(get_succ[context_](f1hs(s2_phi))).reshape(
                    [2, replay_size_, action_dim, phi_dim])

                a2 = randargmax(np.tensordot(both_succs[1, :, :, :phi_pos_size], w.get_value()[
                                context_], [[2], [0]]), axis_=1, squeeze=0)

                target_ = s2_phi + gamma * (np.outer((1 - isterminal),
                                                     np.ones(phi_dim)).reshape([replay_size_,
                                                                                phi_dim])) * np.squeeze([both_succs[1][k,
                                                                                                                       a2[k],
                                                                                                                       :] for k in xrange(replay_size_)])

                cost_s, errors = update_succ[context_](s1_phi.reshape([1, replay_size_, phi_dim]), ac.reshape(
                    [1, replay_size_]), f1hs(target_), 0, weights_[context_], a(succ_lr))

    else:
        # Direct replay of last experience
        s1, ac, s2, s1_phi, s2_phi, r, cr, isterminal = memory[context_].get_last()
        if learn_w:
            rocst_r, rerror_r = update_w_phi(
                s2_phi[:, :phi_pos_size], f1hs(r), w_lr)

            All_costs.append([rerror_r])
        else:
            rerror_r = [[1.]]

        last_pos = (memory[context_].pos[0], memory[context_].pos[1])

        memory[context_].priority[last_pos] = 1


def perform_learning_step(
        s1,
        episode_,
        weights_,
        k_,
        rrewards_,
        got_key,
        cum_state,
        fixed_exploration=0):
    """ Makes an action according to eps-greedy policy, observes the result
    (next state, reward) and learns from the transition"""

    def exploration_rate(epis_):
        """# Define exploration rate change over time"""
        start_eps = 1.
        end_eps = g_epsilon
        const_eps_episodes = 25  # 0.1 * epochs  # 10% of learning time
        eps_decay_episodes = 250  # 0.6 * epochs  # 60% of learning time

        if epis_ < const_eps_episodes:
            return start_eps
        elif epis_ < eps_decay_episodes:
            # Linear decay
            return start_eps - (epis_ - const_eps_episodes) / \
                               (eps_decay_episodes - const_eps_episodes) * (start_eps - end_eps)
        else:
            return end_eps

    randomness = 0
    sampled_context = np.random.choice(k_,p=softmax(weights_[:k_], 1))
    s_features = get_s_features(f1hs(s1, 1))

    exp_epsilon = exploration_rate(
        episode_) * (fixed_exploration == 0) + fixed_exploration

    if np.random.rand() < exp_epsilon:
        action = np.random.choice(action_dim)
        sampled_context = sample(range(MC), 1)[0]
        randomness = 1
        q_values = np.zeros([action_dim, 1])
    else:

        action, q_values = get_action(
            s_features[0][:phi_pos_size], get_succ, w.get_value(), sampled_context)

    observation, reward, terminal, _ = env.step(action)
    rrewards_.append(reward)
    s_reward_ = (np.convolve(rrewards_, reward_convolve)[
                 int(len(reward_convolve) / 2):int(len(reward_convolve) / 2 + len(rrewards_))])
    s2 = f1hs(observation)

    cum_state = cum_state * gamma_cum_state + s2

    memory[sampled_context].add_transition(s1, action, s2, s_features[0], get_s_features(
        f1hs(s2))[0], cum_state, reward, s_reward_[-1], terminal, sampled_context)

    for k in xrange(MC):
        memory[k].ep_lengths[memory[k].pos[0]] = memory[k].pos[1] + 1
    update_weights=f1h(0,randargmax(weights),[1,MC]).astype(theano.config.floatX)
    if direct_update:
        learn_from_memory(sampled_context, 1,update_weights, cum_state)

    if replay_update:

        learn_from_memory(np.argmax(update_weights), 0,update_weights, cum_state)

    k = 0

    return action, s2, reward, terminal, randomness, q_values, got_key, cum_state


def get_action(state_, get_succ_, w_, sampled_context_):
    q_values_ = []
    for k in xrange(MC):
        get_suckers = get_succ_[k](state_.reshape([1, 1, phi_dim]))
        q_values_.append(np.dot(a(get_suckers[0]).reshape(
            [action_dim, phi_dim])[:, :phi_pos_size], w_[k].T))

    return randargmax(q_values_[sampled_context_]), q_values_


def get_reward_pos(pos_=env.rewards[0, :2]):
    return get_reward(env.firing_rates(location_=pos_)[
                      :phi_pos_size].reshape([1, 1, -1]), np.zeros([1, 1]), [1])


def grp(x_, y_):
    return get_reward_pos([x_, y_])[0]


def get_rpf(res_):
    xaxis = np.linspace(0, env.maze_length, res_)
    yaxis = np.linspace(env.maze_length, 0, res_)
    plt.imshow(np.vectorize(grp)(*np.meshgrid(xaxis, yaxis, indexing='xy')))


def get_rp(res_):
    xaxis = np.linspace(0, env.maze_length, res_)
    yaxis = np.linspace(env.maze_length, 0, res_)
    Vs = np.zeros([res_, res_])
    for k_ in xrange(res_):
        for kk_ in xrange(res_):
            Vs[k_, kk_] = get_reward_pos([xaxis[kk_], yaxis[k_]])[0]

    plt.imshow(Vs)
    return Vs


def get_rp_w(res_, p_n):
    w_ = paths[p_n]['w'][0]
    xaxis = np.linspace(0, env.maze_length, res_)
    yaxis = np.linspace(env.maze_length, 0, res_)
    Vs = np.zeros([res_, res_])
    for k_ in xrange(res_):
        for kk_ in xrange(res_):
            Vs[k_, kk_] = np.dot(env.firing_rates(
                location_=[xaxis[kk_], yaxis[k_]]), w_)


def get_rp_w_(res_, w1_=None, pathn=None):
    if pathn is not None:
        w1_ = paths[pathn[0]]['w_cr'][pathn[1]]
    xaxis = np.linspace(0, env.maze_length, res_)
    yaxis = np.linspace(env.maze_length, 0, res_)
    Vs = np.zeros([res_, res_])
    for k_ in xrange(res_):
        for kk_ in xrange(res_):
            Vs[res_ - 1 - k_,
               kk_] = np.dot(env.firing_rates(location_=[xaxis[kk_],
                                                         yaxis[k_]]),
                             w1_)

    plt.imshow(Vs, origin='lower')
    plt.show()
    return Vs


def disp_vs(pathn_=0, w_cr_=None, res_=30):
    Vs = []
    if w_cr_ is not None:
        for k in xrange(MC):
            Vs.append(get_rp_w_(res_, w1_=w_cr_[k]))
            print(np.min(Vs[k]), np.max(Vs[k]))
    else:
        for k in xrange(MC):
            Vs.append(get_rp_w_(res_, pathn=[pathn_, k]))
            print(np.min(Vs[k]), np.max(Vs[k]))
    return Vs


def get_quarter(position_):
    return 2 * int(position_[0] >= env.maze_length / 2.) + \
        int(position_[1] >= env.maze_length / 2.)


def get_corner(position_):

    quarter_ = get_quarter(position_)
    corners = [[0., 0.], [0, env.maze_length], [
        env.maze_length, 0.], [env.maze_length, env.maze_length]]
    return corners[(quarter_ + 1) % 4]


Rand_envs = np.load('Rand_envs_storage_3_long.npy')


All_Returns = []
All_Episode_lengths = []
All_costs = []
All_Weightsr = []
All_weights_per_all_ep = []
Weights_ = []
All_all_rewards = []
All_wsors = []
All_epsilons = []
for epoch in range(epochs):
    wsor = [0.01, 0.01]
    g_epsilons = [0.3, 0.20,.25,.35]
    g_epsilon = g_epsilons[0]
    w_set_off_rate = wsor[epoch % 2]
    All_wsors.append(w_set_off_rate)
    All_epsilons.append(g_epsilon)
    wr = 0.00001 * np.ones([MC, phi_pos_size])
    w = theano.shared(wr)
    w_cr_r = 0.00001 * np.random.randn(MC, phi_pos_size)
    w_cr = theano.shared(w_cr_r)

    actions = [list(ac) for ac in np.eye(4)]
    networks = []
    # Create replay memory which will store the transitions
    memory = []
    memory_cr = []
   
    for k in xrange(MC):
        memory.append(
            ReplayMemory(
                capacity=[
                    replay_memory_size,
                    max_steps_per_episode],
                context=k))
        memory_cr.append(
            ReplayMemory(
                capacity=[
                    replay_memory_size,
                    max_steps_per_episode],
                context=k))
    update_w_phi, update_cr, update_cr_phi, update_phi, get_s_features, get_s_2_features, get_reward, get_cr = create_state_networks()

    get_succ = []
    update_succ = []
    succ_networks = []
    succ_2_networks = []
    for k in xrange(MC):
        succ_network, succ_2_network, g_, u_ = create_succ_networks(
            action_dim, phi_dim)
        get_succ.append(g_)
        update_succ.append(u_)
        succ_networks.append(succ_network)
        succ_2_networks.append(succ_2_network)
        networks.append(succ_network)
        networks.append(succ_2_network)

    total_train_episodes_finished = 0
    time_start = time()

    # =============================================================================
    # # ======================================================================
    # # Starts here!
    # =============================================================================
    # =============================================================================
    weights = np.ones(MC) * 1. / MC
    particles = np.random.choice(MC, [n_particles, len_particles])

    paths = []
    weights_per_all_ep = []
    goal = 0
    reg_cr_phi = 0.0005
    Episode_lengths = []
    Returns = []
    w_s = []
    Errors_cr = []
    Envs_history = []
    nans = []
    all_rewards = []

    phi_lr *= .99
    train_episodes_finished = 0
    train_scores = []
    Weightsr = []

    learning_episodes_per_epoch = 5000
    m_pos = [0, 0]

    for k in xrange(MC):
        for param1, param2 in zip(
            get_all_params(
                succ_2_networks[k]), get_all_params(
                succ_networks[k])):
            param1.set_value(param2.get_value())

    for learning_episode in trange(learning_episodes_per_epoch):
        cr_lr = np.max([cr_lr_min, lr_cr * (1 - wcr_decay * \
                       a(total_train_episodes_finished) / w_cr_decay_rate)])

        lec = learning_episode
        if lec % change_environment == 0:
            env.pos_start = Rand_envs[int(
                len(Rand_envs) / 2 + lec / change_environment)]

            env.set_rewards(0, Rand_envs[int(lec / change_environment)])
            if puddles:

                corner_ = get_corner(env.rewards[0])
                print('lec', lec, a(corner_))
                env.set_rewards(1, a(corner_))
            Envs_history.append(env.rewards.copy())

        w_real = w.get_value()

        w_cr_real = w_cr.get_value()
        w.set_value(w_real + 1. * w_set_off_rate * set_off_w)
        print('Episode', learning_episode)  # ,'Costs:',c1,c2,c3)
        robservations = []
        robservations_r = []
        rrewards = []
        ractions = []
        ractions_r = []
        rstates = []
        rq_values = []
        rcum_states = []
        cors = []
        steps_taken = 0
        rterminals = []
        steps_per_episode = 0
        terminal = 0
        weights_per_ep = []

        got_key = [0, 0, 0]
        env.reset()
        rpos = [env.pos]
        rthetas = [env.theta]
        s1 = f1hs(env.state)
        cum_state = s1
        rcum_states.append(cum_state)
        weights_per_ep = []
        while (steps_per_episode < max_steps_per_episode and not terminal):
            action, s2, reward, terminal, random_step, q_values, got_key, cum_state = perform_learning_step(
                s1, learning_episode, weights, MC, rrewards, got_key, cum_state)

            all_rewards.append(reward)
            steps_per_episode += 1
            steps_taken += 1
            rstates.append(s1)
            rpos.append(env.pos)
            rthetas.append(env.theta)
            rterminals.append(terminal)
            # rall_states.append(s2)
            ractions.append(action)
            robservations.append(s2)
            rcum_states.append(cum_state)
            rq_values.append(q_values)
            if random_step:
                robservations_r.append(s2)

            s_reward_ = (np.convolve(rrewards, reward_convolve)[
                         int(len(reward_convolve) / 2):int(len(reward_convolve) / 2 + len(rrewards))])

            if learning_episode >= start_filtering and len(
                    rrewards) >= filter_delay:
                weights, particles, filtered = pfilter(
                    robservations, rrewards, terminal, s_reward_, w_cr.get_value(), particles)
                weights_per_ep.append(weights)

            if steps_taken % network_synch_rate == 0:
                for k in xrange(MC):
                    for param1, param2 in zip(
                        get_all_params(
                            succ_2_networks[k]), get_all_params(
                            succ_networks[k])):
                        param1.set_value(param2.get_value())

            s1 = s2
            if terminal or steps_per_episode == max_steps_per_episode - 1:
                for k in xrange(MC):
                    memory[k].size = min(
                        memory[k].size + 1, memory[k].capacity[0] - 1)
                    memory[k].pos[1] = 0
                    memory[k].pos[0] = (
                        memory[k].pos[0] + 1) % memory[k].capacity[0]
    # Increments memory position
            else:
                for k in xrange(MC):
                    memory[k].pos[1] = (memory[k].pos[1] + 1)

            if terminal:
                break

        train_episodes_finished += 1
        total_train_episodes_finished += 1
        Weightsr.append(weights)

        returns = []
        return_so_far = 0
        for t in range(len(rrewards) - 1, -1, -1):

            return_so_far = rrewards[t] + gamma * return_so_far
            returns.append(return_so_far)

        rreturns = a(returns[::-1])
        paths.append(dict(
            states=rstates,
            observations=robservations,
            actions=ractions,
            rewards=rrewards,
            crewards=s_reward_,
            steps=steps_per_episode,
            observations_r=robservations_r,
            pos=rpos,
            thetas=rthetas,
            terminals=rterminals,
            cum_states=rcum_states,
            q_values=rq_values,
            returns=rreturns[0],
            env_rewards=env.rewards.copy(),
            w=w.get_value(),
            w_cr=w_cr.get_value(),
        ))
        Episode_lengths.append(steps_per_episode)
        Returns.append(rreturns[0])
       
        print(
            'Episode',
            learning_episode,
            'return:',
            rreturns[0],
            'Weights:',
            weights)
        weights_per_all_ep.append(weights_per_ep)

        for k in xrange(len(robservations)):
            if learn_w:
                rocst_r, rerror_r = update_w_phi(
                    (robservations[k]), f1hs(a([rrewards[k]])), w_lr)
        paths[-1]['w_cr'] = w_cr.get_value()

        print(
            'Total Episode lengths and Returns on iteration',
            learning_episode,
            np.sum(Episode_lengths),
            np.sum(all_rewards))

    All_Episode_lengths.append(Episode_lengths)
    All_Returns.append(Returns)
    All_Weightsr.append(Weightsr)
    All_weights_per_all_ep.append(weights_per_all_ep)
    All_all_rewards.append(all_rewards)
