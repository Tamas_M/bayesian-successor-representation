#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Code for Bayesian Successor Representation
@author: Tamas Madarasz
"""
''' Grid world environment with walls '''
from independent_grid_world import GridWorldEnv
env=GridWorldEnv(desc='8x8_safe')

import numpy as np
from scipy.stats import norm 
import pickle
import matplotlib.pyplot as plt
from matplotlib import cm
a=np.asarray
get_s_features=lambda x:x

'''Experiment 1 settings'''
give_w=1
set_off_w=0
puddles1=0
'''
Experiment 2 settings
give_w=0
set_off_w=1
puddles1=1
'''
# =============================================================================
# Environment params
# =============================================================================
n_rows=env.n_row
n_columns=env.n_col
state_dim=n_rows*n_columns
l=n_rows-1
n_action=4
max_steps_per_episode=75
change_environment=30
fixed_start_state=0
step_penalty=0.0
n_iter=4500
puddle_penalty=-1.
gamma=0.99
# =============================================================================
# Filtering params
# =============================================================================
'''MC: maximum number of clusters'''
MC=4
'''alpha: concentration parameter for Dirichlet process'''
alpha=2.
'''particle_sd: standard deviation for the Gaussian mixtture distributtions'''
particle_sd=1.6
'''number of particles'''
n_particles=100
'''length of each particle (number of steps it keeps previous contexts for'''
len_particles=10
'''Determines the episode when the filtering process starts: set 0 for BSR or a large number to turn off the inference process'''
start_filtering=0*1e10
start_wcr_update=0
wcr_update=1
filter_delay=3
filter_together=1
used=0
equal_weights=0
known_quarter_weights=0
known_quarter_weight=1.
use_q_values=0
# =============================================================================
# CR params
# =============================================================================
lr_cr=.15
w_cr_decay_rate=6000.
wcr_decay=1
w_set_off_rate=0.01
reward_convolve=gamma**a([3,2,1,0,1,2,3])
softmax_on=0
direct_update=1
replay_update=1
replay_size=5
use_terminal_in_replay=0
# =============================================================================
# Learning params
# =============================================================================
'''Not using target value network/function'''
target_network_update=0
'''For q learning set to 1 (double q_learning optional)'''
q_learning=0
double_q=0
'''For GPI use succ_max (choose maximum q_vale) and set signal_change to 1'''
succ_max=0
signal_change=0
'For BSR use succ_learning'''
succ_learning=1
'''If iterating over different learning_rates'''
succ_learning_rates=[0.001,0.005,0.01,0.05,0.1]
'''For annealed (linearly decreasing) exploration rate for the first n episodes'''
epsilon_decay=250.


All_Episode_lengths = []
All_Returns = []
All_Weightsr = []
All_weights_per_all_ep = []
All_trajectories = []
All_epsilons = []
All_learning_rates = []

def list_flatten(l): return [item for sublist in l for item in sublist]

def rtm(pos):
    return pos / n_rows, pos % n_rows

def mtr(pos):
    return pos[0] * n_rows + pos[1]

def softmax(x, theta):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x * theta - np.max(x))
    return e_x / e_x.sum()

def randargmax(b, **kw):
    """ a random tie-breaking argmax"""
    return np.argmax(np.random.random(b.shape) * (b == b.max()), **kw)

def set_environment(start_s, goal_s):
    env.desc[np.where(env.desc == 'G')] = 'F'
    env.desc[np.where(env.desc == 'S')] = 'F'
    env.desc[rtm(start_s)] = 'S'
    env.desc[rtm(goal_s)] = 'G'
    env.start_state = start_s

def get_quarter(goal_s):
    return sum([1, 2] * ((a(rtm(goal_s))) / [n_rows / 2, n_columns / 2]))

def m1h(row, column, sizes):
    oh = np.zeros(sizes).astype('int32')
    oh[row, column] = 1
    return oh

def f1h(row, column, sizes):

    return np.ndarray.flatten(m1h(row, column, sizes))

def f1hs(state):
    o = np.zeros(state_dim).astype('int32')
    o[state] = 1
    return o

def f1ha(action):
    o = np.zeros(n_action).astype('int32')
    o[action] = 1
    return o

def get_new_particle(assignment):
    l = len(assignment)
    particles_ = list(set(assignment))
    pl = len(particles_)
    new_prob = alpha / (alpha + l)
    counts = [assignment.count(k) for k in particles_]
    if pl < MC:
        probs = np.append((a(counts) / (l + alpha)), [new_prob])
        particles_.append(
            min(list(set(range(MC)).difference(set(assignment)))))
    else:
        probs = (a(counts) + alpha / pl) / (l + alpha)
    new_particle = np.random.choice(particles_, p=probs)
    assignment = np.append(a(assignment), [new_particle])
    return new_particle.astype('int32')

def particle_filter(weights_f, particles_):
    new_particles = []
    for k in xrange(n_particles):
        new_particle = get_new_particle(np.ndarray.tolist(particles_[k, :]))
        new_particles.append(new_particle)
    weights_e = a(weights_f)
    weights_p = weights_e / np.sum(weights_e)
    if np.any(np.isnan(weights_p)):
        nans.append(iterations)
        weights_p = f1h(0, randargmax(weights_p), [1, MC])
    unique, counts = np.unique(new_particles, return_counts=True)
    Weights_.append(weights_f)
    counts_t = np.zeros(MC)
    counts_t[unique] = counts
    total_particle_weights = weights_p * counts_t
    weights_t = total_particle_weights / np.sum(total_particle_weights)
    particle_weights = weights_p[new_particles] / \
        np.sum(total_particle_weights)
    particles_ = np.concatenate(
        (particles_, a(new_particles).reshape([n_particles, 1])), axis=1)
    if particles_.shape[1] > len_particles:
        particles_ = np.delete(particles_, 0, 1)
    indices = np.random.choice(n_particles, n_particles, p=particle_weights)
    particles_resampled = particles_[indices[:]]
    return weights_t, particles_resampled

def q_update(
                Q_,
                Q2_,
                state_,
                action_,
                state_p_,
                reward_,
                terminal_,
                weights_,
                q_lr_):
            if double_q:
                
                for k_ in xrange(MC):
                    action_p_ = randargmax(Q_[k_, state_p_, :])
                    Q_[k_, state_, action_] += weights_[k_] * q_lr_[k_] * (reward_ + (
                        1 - terminal_) * gamma * Q2_[k_, state_p_, action_p_] - Q_[k_, state_, action_])
                

            else:
                for k_ in xrange(MC):
                    action_p_ = randargmax(Q_[k_, state_p_, :])
                    Q_[k_, state_, action_] += weights_[k_] * q_lr_[k_] * (reward_ + (
                        1 - terminal_) * gamma * Q_[k_, state_p_, action_p_] - Q_[k_, state_, action_])
            return Q_

def get_action_sample_q(Q_, state_, weights_, k_):

    q_values_r = [Q_[kk_, state_, :] for kk_ in xrange(MC)]
    sampled_context = np.random.choice(k_, p=weights_[:k_])
    q_values_chosen = q_values_r[sampled_context]
    if np.random.rand() < max(epsilon, 1 - a(iterations) / epsilon_decay):
        action = np.random.choice(n_action)
        randomness = 1
    else:
        randomness = 0
        action = np.argmax(q_values_chosen)
    return action, q_values_r, randomness, sampled_context

def succ_update(
        M_,
        M2_,
        w_,
        state_,
        action_,
        state_p_,
        reward_,
        terminal_,
        weights_,
        sf_lr_,
        succ_max_=0):
    for k_ in xrange(MC):
        if weights_[k_] > 0:

            action_p_ = randargmax(np.tensordot(
                M2_[k_, state_p_, :, :], w_[k_, :], (0, 0)))

            M_[k_, state_, :, action_] += weights_[k_] * sf_lr_[k_] * (f1hs(state_p_) + (
                1 - terminal_) * gamma * M2_[k_, state_p_, :, action_p_] - M_[k_, state_, :, action_])
    return M_

def update_w(w_, state_, reward_, weights_, w_lr_):
    for k_ in xrange(MC):

        w_[MC, state_] += weights_[k_] * \
            w_lr_(reward_ - w_[MC, state_])

def update_wcr(w_cr_, s_, sr_, w_nl_, lr_):
    err_ = sr_ - np.dot(s_, w_cr_[w_nl_])
    w_cr_[w_nl_] += lr_ * 2 * err_ * s_
    return (w_cr_)

def get_action_sample(M_, w_, state_, weights_, k_, succ_max_=0):
    if succ_max_ == 0:
        q_values_r = [np.tensordot(M_[kk_, state_, :, :], w_[
                                   kk_, :], (0, 0)) for kk_ in xrange(MC)]
        # q_values_s=a(get_q_values_stable([],state_))
        sampled_context = np.random.choice(k_, p=weights_[:k_])
        q_values_chosen = q_values_r[sampled_context]

        if np.random.rand() < max(epsilon, 1 - a(iterations) / epsilon_decay):
            action = np.random.choice(n_action)
            randomness_ = 1
        else:
            randomness_ = 0
            action = np.argmax(q_values_chosen)

        return action, q_values_r, randomness_, sampled_context
    else:

        q_values_r = [np.tensordot(M_[kk_, state_, :, :], w_[
                                   current_context, :], (0, 0)) for kk_ in xrange(MC)]
        best_q = np.argmax(q_values_r)
        action = best_q % n_action
        winning_context = best_q / n_action

        if np.random.rand() < max(epsilon, 1 - a(iterations) / epsilon_decay):
            action = np.random.choice(n_action)
            randomness_ = 1
        else:
            randomness_ = 0
        return action, q_values_r, randomness_, winning_context

# =============================================================================
# Experiments start here, kk counts number of repeats of experiment,  cycling through learning, rates exploration rates
# =============================================================================
for kk in xrange(250):
    epsilon = (kk / 50) * 0.05
    All_epsilons.append(epsilon)

    w_cr_r = 0.0001 * np.random.randn(MC, state_dim)
    w_cr = w_cr_r.copy()
    w = 0.001 * np.random.randn(MC, state_dim)
    if q_learning:
        Q = 0.0001 * np.ones([MC, state_dim, n_action])
        Q2 = Q.copy()

    if succ_learning:
        M = np.zeros([MC, state_dim, state_dim, n_action])
        M2 = M.copy()
        w = 0.0001 * np.random.randn(MC, state_dim)
        succ_updates = []  

    Episode_lengths = []
    Returns = []
    weights = np.ones(MC) * 1. / MC
    weights_nl = weights
    particles = np.random.choice(MC, [n_particles, len_particles])
    learning_rate_succ = np.ones(MC)* succ_learning_rates[(kk/10)%50]
    All_learning_rates.append(learning_rate_succ)
    learning_rate_q = np.ones(MC) * 0.05

    trajectories = []
    paths = []
    Weightsr = []
    costs = []
    cost_ts = []
    Envs_history = []
    nans = []
    Weights_ = []
    e_database = [[] for k in xrange(MC)]
    weights_per_all_ep = []
    ################################   The action starts here  ###############
    with open('Random_envs_storage_8_safe') as f:
        Rand_envs = pickle.load(f)
        
    for iterations in xrange(n_iter):

        if set_off_w:
            if succ_learning:
                w = w + ((w_cr + 1.) * w_set_off_rate * set_off_w)

        if iterations % change_environment == 0:
            start_s, goal_s, env_rewards = Rand_envs[iterations /
                                                     change_environment + 1]
            '''Corrects reward value to 10.'''
            env_rewards = env_rewards * 10. / 10.001
            if puddles1:
                for state_ in xrange(state_dim):
                    if get_quarter(
                            state_) + get_quarter(goal_s) == 3 and env.desc[rtm(state_)] != 'W':
                        env_rewards[state_] = puddle_penalty
            set_environment(start_s, goal_s)
            if give_w:
                if succ_max:
                    if used < MC - 1:

                        current_context = used
                        used += 1
                    else:
                        current_context = np.random.choice(MC)
                    e_database[current_context] = []
                    w[current_context, :] = env_rewards
                    weights = np.zeros(MC)
                    weights[current_context] = 1
                else:
                    w = np.tile(env_rewards, (MC, 1))

            if signal_change:
                if used < MC - 1:

                    current_context = used
                    used += 1
                else:
                    current_context = np.random.choice(MC)
                e_database[current_context] = []
                weights = np.zeros(MC)
                weights[current_context] = 1
                w[current_context] = 0.0001 * (np.random.randn(state_dim))
            
            if iterations < start_filtering:
                if known_quarter_weights:
                    weights = (1. - known_quarter_weight) / 3. * np.ones(MC)
                    weights[get_quarter(goal_s)] = known_quarter_weight
                    weights_nl = weights

                if equal_weights:
                    weights = 1. / a(MC) * np.ones(MC)
                    weights_nl = 1. / a(MC) * np.ones(MC)

            Envs_history.append([start_s, goal_s])
        if target_network_update and iterations % target_network_update == 0:
            if q_learning:
                Q2 = Q.copy()
            if succ_learning:
                M2 = M.copy()

        robservations = []
        robservations_r = []
        rrewards = []
        ractions = []
        ractions_r = []
        rstates = []
        rq_values = []
        state = env.reset()
        steps_per_episode = 0
        rall_states = [state]
        weights_per_ep = []

        while (steps_per_episode < max_steps_per_epsiode):
            if succ_learning:
                action, q_values_r, randomness, sampled_context = get_action_sample(
                    M, w, state, weights, MC, succ_max_=succ_max)

            if q_learning:
                if double_q:
                      
                            action, q_values_r, randomness, sampled_context = get_action_sample_q(
                    (Q+Q2)/2, state, weights, MC)
                  
                         
                else:
                    action, q_values_r, randomness, sampled_context = get_action_sample_q(
                    Q, state, weights, MC)

            observation, reward, terminal, _ = env.step(action)
            reward = env_rewards[observation] - step_penalty
            steps_per_episode += 1
            rq_values.append(q_values_r)
            robservations.append(observation)
            rall_states.append(observation)
            ractions.append(action)
            robservations_r.append(observation)
            ractions_r.append(action)
            rstates.append(state)
            if randomness is 1:
                robservations_r.append('random')
                ractions_r.append('random')
            rrewards.append(reward)
            new_particles = []
            particle_weights = []

            e_database[sampled_context].append(dict(
                s=state,
                a=action,
                r=reward,
                s_next=observation,
                w_nl=weights_nl,
                sc=sampled_context,
                t=terminal))
            if len(e_database[sampled_context]) > 300:
                e_database[sampled_context].pop(0)


# =============================================================================
#            UPDATES
# =============================================================================

            s_reward_ = (
                np.convolve(
                    rrewards,
                    reward_convolve)[
                    len(reward_convolve) / 2:len(reward_convolve) / 2 + len(rrewards)]) / (
                np.convolve(
                    np.ones(
                        len(rrewards)),
                    reward_convolve)[
                    len(reward_convolve) / 2:len(reward_convolve) / 2 + len(rrewards)])
            if succ_learning:

                if succ_max:
                    w[current_context, observation] = reward
                else:
                    w[:, observation] = reward
            '''Update all maps'''
            weights_nl=np.ones(MC)/a(MC,dtype='float32')  
            '''or update only the most liekly map'''
            #weights_nl=f1h(0,randargmax(weights),[1,MC])            
            if direct_update:
                if succ_learning:

                    if succ_max:
                        weights_nl = np.zeros(MC)
                        weights_nl[sampled_context] = 1
                        weights_nl[current_context] = 1
                    M = succ_update(M, M, w, state, action, observation, a(reward).astype(
                        'float32'), a(terminal).astype('int32'), weights_nl, learning_rate_succ)

                if q_learning:
                    if double_q:
                        if np.random.rand()>=0.5:
                           Q = q_update(Q, Q2, state, action, observation, a(reward).astype(
                                'float32'), a(terminal).astype('int32'), weights_nl, learning_rate_q)
                        else:
                            Q2=q_update(Q2, Q, state, action, observation, a(reward).astype(
                                'float32'), a(terminal).astype('int32'), weights_nl, learning_rate_q)
                    else:
                        Q = q_update(Q, Q2, state, action, observation, a(reward).astype(
                        'float32'), a(terminal).astype('int32'), weights_nl, learning_rate_q)

            if replay_update:
                if succ_max:
                    replay_indeces = np.random.choice(len(e_database[sampled_context]), min(
                        replay_size, len(e_database[sampled_context])))
                else:
                    replay_indeces = np.random.choice(len(e_database[sampled_context]), min(
                        replay_size, len(e_database[sampled_context])))

                for r_index in replay_indeces:
                    memory = e_database[sampled_context][r_index]
                    k = memory['sc']
                    weights_nl_replay = f1h(0, memory['sc'], [1, MC])
                    if succ_learning:

                        M = succ_update(
                            M, M, w, memory['s'], memory['a'], memory['s_next'], a(
                                memory['r']).astype('float32'), a(
                                memory['t'] * use_terminal_in_replay).astype('int32'), weights_nl, learning_rate_succ)

                    if q_learning:
                        Q = q_update(
                            Q, Q2, memory['s'], memory['a'], memory['s_next'], a(
                                memory['r']).astype('float32'), a(
                                memory['t'] * use_terminal_in_replay).astype('int32'), weights_nl, learning_rate_q)

# =============================================================================
# FILTERING!
# =============================================================================
            weights_ = []
            if iterations >= start_filtering:
                if not terminal and len(rrewards) >= 3:
                    filter_ = 1

                    s_state = robservations[-filter_delay]

                    s_reward = s_reward_[-filter_delay]
                    # s_state=robservations[-3]
                    # s_reward=rrewards[-3]

                    for k in xrange(MC):
                        weights_.append(norm.pdf(s_reward, np.dot(
                            get_s_features(f1hs(s_state)), w_cr[k, :]), particle_sd))

                    weights, particles = particle_filter(weights_, particles)
                    weights_per_ep.append(weights)

                    weights_nl_2 = f1h(
                        0, randargmax(weights), [
                            1, MC]).astype('float32')

                    learning_rate_cr = lr_cr * \
                        (1 - wcr_decay * a(iterations) / w_cr_decay_rate)

                    if iterations >= start_wcr_update and wcr_update:

                        w_cr = update_wcr(
                            w_cr,
                            f1hs(s_state),
                            s_reward,
                            randargmax(weights),
                            learning_rate_cr)
                elif terminal:
                    filter_ = 1
                    s_reward = s_reward_[-min(len(rrewards), filter_delay):]      
                    cr_list = zip(robservations[-len(s_reward):], s_reward)              
                    if filter_together:
                        for k in xrange(MC):
                            weights_.append(np.exp(np.sum([np.log(norm.pdf(el[1], np.dot(
                                get_s_features(f1hs(el[0])), w_cr[k, :]), particle_sd)) for el in cr_list])))
                        weights, particles = particle_filter(
                            weights_, particles)
                        weights_per_ep.append(weights)
                        weights_nl_2 = f1h(
                            0, randargmax(weights), [
                                1, MC]).astype('float32')
                        learning_rate_cr = lr_cr * \
                            (1 - wcr_decay * a(iterations) / w_cr_decay_rate)
                        if iterations >= start_wcr_update and wcr_update:
                            for el in cr_list:
                                w_cr = update_wcr(
                                    w_cr,
                                    f1hs(
                                        el[0]),
                                    el[1],
                                    randargmax(weights),
                                    learning_rate_cr)
                    else:
                        for el in cr_list:
                            weights_ = []
                            for k in xrange(MC):
                                weights_.append(np.exp(norm.pdf(el[1], np.dot(
                                    get_s_features(f1hs(el[0])), w_cr[k, :]), particle_sd)))
                            weights, particles = particle_filter(
                                a(weights_), particles)
                            weights_per_ep.append(weights)
                            weights_nl_2 = f1h(
                                0, randargmax(weights), [
                                    1, MC]).astype('float32')
                            learning_rate_cr = lr_cr * \
                                (1 - wcr_decay * a(iterations) / w_cr_decay_rate)
                            if iterations >= start_wcr_update and wcr_update:
                                w_cr = update_wcr(
                                    w_cr,
                                    f1hs(
                                        el[0]),
                                    el[1],
                                    randargmax(weights),
                                    learning_rate_cr)
                else:
                    filter_ = 0
            state = observation
            if terminal:
                break

# =============================================================================
# Store episode details in paths
# =============================================================================
        s_reward = (
            np.convolve(
                rrewards,
                reward_convolve)[
                len(reward_convolve) / 2:len(reward_convolve) / 2 + len(rrewards)]) / (
            np.convolve(
                np.ones(
                    len(rrewards)),
                reward_convolve)[
                len(reward_convolve) / 2:len(reward_convolve) / 2 + len(rrewards)])

        returns = []
        return_so_far = 0

        qs_ = [[] for k in xrange(MC)]
        for t in range(len(rrewards) - 1, -1, -1):
            return_so_far = rrewards[t] + gamma * return_so_far
            returns.append(return_so_far)
        returns = returns[::-1]
        weights_per_all_ep.append(weights_per_ep)
        w_real = w
        paths.append(dict(
            states=rstates,
            observations=robservations,
            actions=ractions,
            rewards=rrewards,
            crewards=s_reward,
            steps=steps_per_episode,
            observations_r=robservations_r,
            actions_r=ractions_r,
            q_values=rq_values,
            returns=returns,
            all_states=rall_states,
            w=w_real,
            w_cr=w_cr,
            epsilon=epsilon,
            learning_rate_succ=learning_rate_succ,
        ))  
        p_last = paths[-1]
        Weightsr.append(a(weights, dtype='float32'))
        Episode_lengths.append(paths[-1]['steps'])
        Returns.append(
            np.sum((gamma**np.arange(len(paths[-1]['rewards'])) * paths[-1]['rewards'])))
       
        print('Average Return on iteration',
              kk,
              iterations,
              np.sum((gamma**np.arange(len(paths[-1]['rewards'])) * paths[-1]['rewards'])),
              np.sum(Episode_lengths),
              np.sum(Returns))
        
        if kk > 0:
            print(np.mean([np.sum(All_Returns[k][:])
                           for k in xrange(len(All_Returns))]))
            print(np.mean([np.sum(All_Episode_lengths[k][:])
                           for k in xrange(len(All_Returns))]))
    print(kk)
    All_Episode_lengths.append(Episode_lengths)
    All_Returns.append(Returns)
    All_Weightsr.append(Weightsr)
    All_weights_per_all_ep.append(weights_per_all_ep)